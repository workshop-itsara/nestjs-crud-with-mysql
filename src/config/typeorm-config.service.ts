import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmOptionsFactory, TypeOrmModuleOptions } from '@nestjs/typeorm';

@Injectable()
export class TypeOrmConfigService implements TypeOrmOptionsFactory {
      constructor(private configService: ConfigService) { }

      createTypeOrmOptions(): TypeOrmModuleOptions {
            return {
                  type: 'mariadb',
                  port: this.configService.get('DATABASE_PORT'),
                  host: this.configService.get('DATABASE_HOST'),
                  username: this.configService.get('DATABASE_USER'),
                  password: this.configService.get('DATABASE_PASSWORD'),
                  database: this.configService.get('DATABASE_NAME'),
                  entities: ['dist/**/*.entity{.ts,.js}'],
                  synchronize: (this.configService.get('APP_MODE') !== 'production')
            };
      }
}
