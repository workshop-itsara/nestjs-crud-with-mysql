import { IsEmail, IsInt, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class UpdateMemberDto {
      @IsOptional()
      @IsNotEmpty()
      @IsString()
      name: string;

      @IsOptional()
      @IsNotEmpty()
      @IsEmail()
      email: string;

      @IsOptional()
      @IsInt()
      age: number;
}