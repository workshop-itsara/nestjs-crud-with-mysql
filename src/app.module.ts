import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypeOrmConfigService } from './config/typeorm-config.service';
import { MemberModule } from './module/member/member.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `env/${(!process.env.NODE_ENV ? 'development' : process.env.NODE_ENV)}.env`
    }),
    TypeOrmModule.forRootAsync({
      inject: [TypeOrmConfigService],
      useClass: TypeOrmConfigService
    }),
    MemberModule
  ]
})
export class AppModule { }
