import { Column, Entity, Generated, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'member' })
export class Member {
      @PrimaryGeneratedColumn({ comment: 'ID ผู้ใช้' })
      id: number;

      @Column({ type: 'varchar', length: 50, unique: true, comment: 'UUID' })
      @Generated('uuid')
      uuid?: string;

      @Column({ type: 'varchar', length: 100, comment: 'ชื่อ' })
      name: string;

      @Column({ type: 'varchar', length: 200, comment: 'อีเมล' })
      email: string;

      @Column({ type: 'int', width: 10, nullable: true, default: 0, comment: 'อายุ' })
      age: number;

      @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP', comment: 'วันที่สร้าง' })
      created_at: Date | String;

      @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP', comment: 'วันที่แก้ไข' })
      updated_at: Date | String;
}