import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, DeleteResult, Repository, SelectQueryBuilder } from 'typeorm';
import { CreateMemberDto, UpdateMemberDto } from './dto';
import { Member } from './entity';
import * as moment from 'moment';

@Injectable()
export class MemberService {
      constructor(
            @InjectConnection() private connection: Connection,
            @InjectRepository(Member) private readonly memberRepository: Repository<Member>
      ) { }

      async getMember(): Promise<object[]> {
            const member: Member[] = await this.memberRepository
                  .createQueryBuilder()
                  .select([
                        `uuid`,
                        `name`,
                        `email`
                  ])
                  .orderBy(`updated_at`, `DESC`)
                  .getRawMany();

            return member;
      }

      async getMemberDetail(uuid: string = ''): Promise<object> {
            const member: Member = await this.memberRepository
                  .createQueryBuilder()
                  .select([
                        `uuid`,
                        `name`,
                        `email`,
                        `age`
                  ])
                  .where(`uuid = :uuid`, { uuid })
                  .getRawOne();
            if (!member) throw new HttpException('ไม่พบข้อมูล', HttpStatus.NOT_FOUND);
            return member;
      }

      async createFarmer(createMemberDto: CreateMemberDto): Promise<object> {
            const queryRunner = this.connection.createQueryRunner();
            await queryRunner.startTransaction();

            try {
                  const member: Member = new Member();
                  member['name'] = createMemberDto['name'];
                  member['email'] = createMemberDto['email'];
                  member['age'] = createMemberDto['age'];
                  await queryRunner.manager.save(member);
                  // throw new HttpException('ERROR', HttpStatus.BAD_REQUEST);
                  await queryRunner.commitTransaction();
                  return {
                        message: 'Member created'
                  };
            } catch (err) {
                  await queryRunner.rollbackTransaction();
                  throw new HttpException(err['response'], err['status']);
            } finally {
                  await queryRunner.release();
            }
      }

      async updateMember(
            uuid: string = '',
            updateMemberDto: UpdateMemberDto
      ): Promise<object> {
            const queryRunner = this.connection.createQueryRunner();
            await queryRunner.startTransaction();

            try {
                  const member: Member = await this.memberRepository.findOne({ uuid });
                  if (!member) throw new HttpException('ไม่พบข้อมูล', HttpStatus.NOT_FOUND);
                  member['name'] = updateMemberDto['name'];
                  member['email'] = updateMemberDto['email'];
                  member['age'] = updateMemberDto['age'];
                  member['updated_at'] = moment().format('YYYY-MM-DD HH:mm:ss');
                  await queryRunner.manager.save(member);
                  await queryRunner.commitTransaction();
                  return {
                        message: 'Member updated'
                  };
            } catch (err) {
                  await queryRunner.rollbackTransaction();
                  throw new HttpException(err['response'], err['status']);
            } finally {
                  await queryRunner.release();
            }
      }

      async deleteMember(uuid: string = ''): Promise<object> {
            const queryRunner = this.connection.createQueryRunner();
            await queryRunner.startTransaction();

            try {
                  const member: Member = await this.memberRepository.findOne({ uuid });
                  if (!member) throw new HttpException('ไม่พบข้อมูล', HttpStatus.NOT_FOUND);
                  await queryRunner.manager.delete(Member, { uuid });
                  await queryRunner.commitTransaction();
                  return {
                        message: 'Member deleted'
                  };
            } catch (err) {
                  await queryRunner.rollbackTransaction();
                  throw new HttpException(err['response'], err['status']);
            } finally {
                  await queryRunner.release();
            }
      }
}
