import { IsEmail, IsInt, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateMemberDto {
      @IsNotEmpty()
      @IsString()
      name: string;

      @IsNotEmpty()
      @IsEmail()
      email: string;

      @IsInt()
      age: number;
}