<div align="center">
    <img src="https://www.vectorlogo.zone/logos/nestjs/nestjs-ar21.svg" width="200">
</div>

# NestJS - CRUD with MySQL

API for Create, Read, Update, Delete with NestJS and MySQL

## Table of Contents

- [Prerequisites](#prerequisites)
- [Configuration](#configuration)
- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

## Prerequisites

* NestJS : 7.5.1
* Node : 18.10.0
* Package Manager (npm) : 8.19.2
* MariaDB : 10.11.3

## Configuration

* Port : 3000

## Installation

Using npm:

```bash
npm install
```

Using docker:

```bash
docker compose up -d
```

## Usage

Using npm:

```bash
npm run start:dev
```

Using docker:

```bash
docker ps --filter name=nestjs_crud_with_mysql
```

## Credits

Itsara Rakchanthuek

## License

[MIT](LICENSE)