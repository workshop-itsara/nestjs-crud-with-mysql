import { Controller, Body, Param, Get, Post, Put, Delete, HttpStatus, HttpCode, HttpException, Patch } from '@nestjs/common';
import { MemberService } from './member.service';
import { CreateMemberDto, UpdateMemberDto } from './dto';

@Controller('member')
export class MemberController {
      constructor(private readonly memberService: MemberService) { }

      @Get('get-member')
      async getMember(): Promise<object[]> {
            return await this.memberService.getMember();
      }

      @Get('get-member-detail/:uuid')
      async getMemberDetail(@Param('uuid') uuid: string): Promise<object> {
            return await this.memberService.getMemberDetail(uuid);
      }

      @Post('create-member')
      async createFarmer(@Body() createMemberDto: CreateMemberDto): Promise<object> {
            return await this.memberService.createFarmer(createMemberDto);
      }

      @Patch('update-member/:uuid')
      async updateMember(
            @Param('uuid') uuid: string,
            @Body() updateMemberDto: UpdateMemberDto
      ): Promise<object> {
            return await this.memberService.updateMember(uuid, updateMemberDto);
      }

      @Delete('delete-member/:uuid')
      async deleteMember(@Param('uuid') uuid: string): Promise<object> {
            return await this.memberService.deleteMember(uuid);
      }
}
