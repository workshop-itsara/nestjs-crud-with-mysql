import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';

export function IsBase64Custom(validationOptions?: ValidationOptions) {
      return (object: Object, propertyName: string) => {
            registerDecorator({
                  name: 'isBase64Custom',
                  target: object.constructor,
                  propertyName: propertyName,
                  options: validationOptions,
                  validator: {
                        validate(value: any, args: ValidationArguments) {
                              let isBase64 = false;

                              if ((typeof value === 'string') && (String(value).split('base64,').length > 1)) {
                                    isBase64 = (Buffer.from(value.split('base64,')[1], 'base64').toString('base64') === value.split('base64,')[1]);
                              }

                              return isBase64;
                        }
                  }
            });
      };
}